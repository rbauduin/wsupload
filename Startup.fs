open System
open Microsoft.AspNetCore.Builder
open Microsoft.Extensions.Hosting
open Microsoft.AspNetCore.Http
open Microsoft.AspNetCore.Hosting
open Microsoft.AspNetCore.Http.Features
open Microsoft.Extensions.DependencyInjection
open WebSharper.AspNetCore
open wsupload

[<EntryPoint>]
let main args =
    let builder = WebApplication.CreateBuilder(args)

    // Add services to the container.
    builder.Services.AddWebSharper()
        .AddAuthentication("WebSharper")
        .AddCookie("WebSharper", fun options -> ())
    |> ignore

    builder.Services.Configure<Microsoft.AspNetCore.Http.Features.FormOptions>( fun (o: FormOptions) -> o.MultipartBodyLengthLimit <- 50000000000L )
    |> ignore
    builder.WebHost.ConfigureKestrel(
        fun o ->
            //o.Limits.MaxRequestBodySize <- 50000000000L
            o.Limits.MaxRequestBodySize <-  System.Nullable()
        )
    |> ignore



    let app = builder.Build()

    // Configure the HTTP request pipeline.
    if not (app.Environment.IsDevelopment()) then
        app.UseExceptionHandler("/Error")
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            .UseHsts()
        |> ignore

    app.UseHttpsRedirection()
        .UseAuthentication()
        .UseStaticFiles()
        .UseWebSharper(fun ws -> ws.Sitelet(Site.Main) |> ignore)
    |> ignore

    app.Run()

    0 // Exit code
