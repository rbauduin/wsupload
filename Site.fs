namespace wsupload

open WebSharper
open WebSharper.Sitelets
open WebSharper.UI
open WebSharper.UI.Server

module Templating =
    open WebSharper.UI.Html

    // Compute a menubar where the menu item for the given endpoint is active
    let MenuBar (ctx: Context<EndPoint>) endpoint : Doc list =
        let ( => ) txt act =
             li [if endpoint = act then yield attr.``class`` "active"] [
                a [attr.href (ctx.Link act)] [text txt]
             ]
        [
            "Home" => EndPoint.Home
            "About" => EndPoint.About
        ]

    let Main ctx action (title: string) (body: Doc list) =
        Content.Page(
            Templates.MainTemplate()
                .Title(title)
                .MenuBar(MenuBar ctx action)
                .Body(body)
                .Doc()
        )

module Site =
    open WebSharper.UI.Html

    open type WebSharper.UI.ClientServer

    let HomePage ctx =
        Templating.Main ctx EndPoint.Home "Home" [
            h1 [] [text "Say Hi to the server!"]
            hr [] []
            div [] [client (Client.rpcForm())]
            hr [] []
            div [] [client (Client.postedForm())]
            hr [] []
            div [] [client (Client.postedAsRecordForm())]
            hr [] []
            div [] [client (Client.ajaxForm())]
            hr [] []
            div [] [client (Client.ajaxMultipleUploadsForm())]
        ]

    let AboutPage ctx =
        Templating.Main ctx EndPoint.About "About" [
            h1 [] [text "About"]
            p [] [text "This is a template WebSharper client-server application."]
        ]


    let HandlePost (ctx: Context<EndPoint>) textField =

        printfn "Text field value was %s" textField
        let uploadPath = Helpers.copyFirstFileTo ctx "/tmp"
        Templating.Main ctx (EndPoint.SubmitForm textField) "Upload By POST" [
            h1 [] [text "Upload By POST"]
            p [] [text (sprintf "Text field had value %s and file was saved to %s" textField uploadPath)]
        ]

    let HandlePostedAsRecord ctx r =
        printfn "Text field value was %s" r.textField
        let uploadPath = Helpers.copyFirstFileTo ctx "/tmp"
        Templating.Main ctx (EndPoint.SubmitForm (r.textField)) "Upload By POST as Record" [
            h1 [] [text "Upload By POST as Record"]
            p [] [text (sprintf "Text field had value %s and file was saved to %s" r.textField uploadPath)]
        ]

    let HandleXhrSingleUpload (ctx: Context<EndPoint>)  =
        let id = Helpers.copyXhrFirstFileToWaitRoom ctx
        Content.Json {| id = id |}

    let HandleXhrMultipleUploads (ctx: Context<EndPoint>)  = async {
        let! id = Helpers.copyXhrAllFilesToWaitRoom ctx
        return! Content.Json {| id = id |}
    }

    [<Website>]
    let Main =
        Application.MultiPage (fun ctx endpoint ->
            match endpoint with
            | EndPoint.Home -> HomePage ctx
            | EndPoint.About -> AboutPage ctx
            | EndPoint.SubmitForm textField -> HandlePost ctx textField
            | EndPoint.SubmitFormAsRecord r -> HandlePostedAsRecord ctx r
            | EndPoint.UploadWithXhr -> HandleXhrSingleUpload ctx
            | EndPoint.MultipleUploadsWithXhr -> HandleXhrMultipleUploads ctx
        )
