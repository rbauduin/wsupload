﻿namespace wsupload

open WebSharper

module Server =

    [<Rpc>]
    let DoSomething input =
        let R (s: string) = System.String(Array.rev(s.ToCharArray()))
        async {
            return R input
        }

    [<Rpc>]
    let HandleRpcForm (textField:string) (fileName:string) (fileContent:byte array) =
        async {
            let uploadPath = System.IO.Path.Combine("/tmp",fileName)
            do! System.IO.File.WriteAllBytesAsync(uploadPath, fileContent) |> Async.AwaitTask
            return (sprintf "Text field value was %s and Uploaded file saved at %s" textField uploadPath)
        }

    [<Rpc>]
    // id is a string because it was truncated and I didn't spend time looking at it
    // sent value was 6531847965349059198, and server side the value used was 6531847965349060000
    let HandleRpcFormAfterAjaxUpload (textField:string) (fileName:string) (id:string) =
        async {
            let uploadPath = System.IO.Path.Combine("/tmp", id , fileName)
            let destinationPath = $"/tmp/final_upload_{fileName}"
            System.IO.File.Copy(uploadPath, destinationPath)
            return (sprintf "Text field value was %s and Uploaded file saved at %s" textField destinationPath)
        }

    [<Rpc>]
    // id is a string because it was truncated and I didn't spend time looking at it
    // sent value was 6531847965349059198, and server side the value used was 6531847965349060000
    let HandleRpcFormAfterAjaxMultipleUploads (textField:string) (fields: string list) (ids:string option list) =
        async {
            let fieldsNumber = List.length fields
            let idsNumber = List.length ids
            if fieldsNumber <> idsNumber then
                return (sprintf "Error! Fields list (%d) and ids list (%d) have different lengths!" fieldsNumber idsNumber)
            else
                return
                    // build string to be displayed by the client in its result area
                    List.fold2
                        (fun acc field (id:string option) ->
                            match id with
                            | Some "" -> sprintf "%s. No upload for field %s." acc field
                            | Some idValue ->
                                sprintf "%s Would copy files ( %s ) in %s for field %s." acc (System.IO.Directory.GetFiles($"/tmp/{idValue}")|> String.concat ",") idValue field
                            | _ ->  sprintf "%s. No upload for field %s but None is not expected here." acc field
                                // If there's no id, there was no upload for this field
                        )
                        $"Text field has value {textField}."
                        fields
                        ids
        }
