﻿namespace wsupload

open WebSharper
open WebSharper.UI
open WebSharper.UI.Templating
open WebSharper.UI.Notation
open WebSharper.UI.Html
// needed for *DynPred attributes
open WebSharper.UI.Client

[<JavaScript>]
module Templates =

    type MainTemplate = Templating.Template<"Main.html", ClientLoad.FromDocument, ServerLoad.WhenChanged>

[<JavaScript>]
module Client =

    let rpcForm () =
        // Var holding the files to upload
        let fileVar:Var<JavaScript.File array> = Var.Create [||]
        let textVar:Var<string> = Var.Create ""
        let resultMessage = Var.Create ""
        let uploadForm =
            div [] [
                Client.Doc.InputType.Text [] textVar
                Client.Doc.InputType.File [] fileVar
                button
                    [ on.click (fun el ev ->
                        let f = fileVar.Get()[0]
                        async {
                            let! buffer = f.ArrayBuffer() |>WebSharper.JavaScript.Promise.AsAsync
                            // Get a Uint8Array from ArrayBuffer
                            let q = WebSharper.JavaScript.Uint8Array(buffer)
                            // Get the bytes for each Uint8Array element
                            // This gives us a byte array we can send to the server side
                            let d = [| for i in 1..q.Length do q.Get(i-1) |]
                            try
                                let! res = Server.HandleRpcForm (textVar.Get()) (f.Name) d
                                resultMessage.Set res
                            with
                            | e -> resultMessage.Set "error"

                            return ()
                        } |> Async.StartImmediate
                        )
                    ]
                    [text "submit by RPC"]
                div [] [text resultMessage.V]
            ]
        uploadForm

    let postedForm () =
        let fileVar:Var<JavaScript.File array> = Var.Create [||]
        let textVar:Var<string> = Var.Create ""
        let uploadForm =
            form [attr.action "/submit_form"; attr.method "POST"; attr.enctype "multipart/form-data"] [
                Client.Doc.InputType.Text [attr.name "textField"] textVar
                Client.Doc.InputType.File [attr.name "uploadedFile"] fileVar
                button
                    []
                    [text "submit by POST"]
            ]
        uploadForm

    let postedAsRecordForm () =
        let fileVar:Var<JavaScript.File array> = Var.Create [||]
        let textVar:Var<string> = Var.Create ""
        let uploadForm =
            form [attr.action "/submit_form_as_record"; attr.method "POST"; attr.enctype "multipart/form-data"] [
                Client.Doc.InputType.Text [attr.name "textField"] textVar
                Client.Doc.InputType.File [attr.name "uploadedFile"] fileVar
                button
                    []
                    [text "submit by POST as Record"]
            ]
        uploadForm

    let ajaxForm () =
        let fileVar:Var<JavaScript.File array> = Var.Create [||]
        let textVar:Var<string> = Var.Create ""
        let resultMessage = Var.Create ""
        let uploadForm =
            div [] [
                text "This form first uploads the file, which the server saves at a temporary location, returning an identification of that location.
                      After that, the form fields are sent via RPC along with the id of the upload, allowing the server to link the upload to the form submission.  "
                br [] []
                Client.Doc.InputType.Text [attr.name "textField"] textVar
                Client.Doc.InputType.File [attr.name "uploadedFile"] fileVar
                button
                    [on.click (fun ev el ->
                        // We start by uploading the file
                        let file = fileVar.Get()[0]
                        JavaScript.Console.Log($"adding {file.Name} to formData")
                        // We need to build a FormData and append the file to be uploaded
                        let formData = JavaScript.FormData()
                        formData.Append(file.Name,file)
                        JavaScript.Console.Log(formData)

                        // We will send an xhr request to upload the file
                        let xhr = new JavaScript.XMLHttpRequest();
                        // When the upload is done, we will issue a RPC call
                        xhr.Onload <-
                            (fun ev ->
                                // This callback is sent when the upload is done
                                let response = xhr.Response
                                // The server returns a simple JSON. We use anonymous records here, but in prod we could/should
                                // use a proper record type shared by client and server.
                                let fileInfo:{|id:string|} = unbox (WebSharper.JavaScript.JSON.Parse (unbox response))

                                // The RPC function is async, so wrap the call in an async {}
                                async{
                                    try
                                        // The RPC returns the string we will display as the result
                                        let! res = Server.HandleRpcFormAfterAjaxUpload (textVar.Get()) (file.Name) (fileInfo.id)
                                        resultMessage.Set res
                                    with
                                    | e -> resultMessage.Set "error"

                                    return ()
                                } |> Async.StartImmediate
                            )
                        let uploadLink = "/upload_with_xhr"
                        // Do not set the content type here, it prevents the browser setting
                        // data boundaries https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects#sending_files_using_a_formdata_object
                        // Now that we have defined all prerequisites like callback and formData, issue the xhr request
                        xhr.Open("post",uploadLink)
                        xhr.Send(formData)
                    )]
                    [text "submit by AJAX"]
                div [] [text resultMessage.V]
            ]
        uploadForm

    let ajaxMultipleUploadsForm () =
        // Vars holding files to be uploaded
        let fileVar:Var<JavaScript.File array> = Var.Create [||]
        let file2Var:Var<JavaScript.File array> = Var.Create [||]
        // Vars holding ids of the uploads for a 2 step form submission
        // When the upload is done, the corresponding Var is set to hold
        // an id returned by the server to identify the uploaded files
        // When no fil is to be uploaded, the id is set to the empty string.
        // Using these Vars in this way they also constitute a signal indicating
        // if the second step of form submission (calling an RPC with other fields values)
        // can be done
        let fileUploadId:Var<string option> = Var.Create None
        let file2UploadId:Var<string option> = Var.Create None
        // Vars holding upload progress
        let progress:Var<int option> = Var.Create None
        let progress2:Var<int option> = Var.Create None
        // Uploads cancel functions
        // The cancel button will call these functions. Cancelling the uploads
        // will also cancel the form submission as submission of other fields
        // is done only when uploads are finished (see xhr.OnLoad)
        let cancelUpload:ref<unit->unit>=ref (fun () -> ())
        let cancelUpload2:ref<unit->unit>=ref (fun () -> ())
        // Var indicating if all uploads are done
        let uploadDone:View<bool> =
            View.Map2
                (fun (v1: string option) (v2: string option) -> (v1.IsSome) && (v2.IsSome) )
                fileUploadId.View
                file2UploadId.View

        let textVar:Var<string> = Var.Create ""
        let resultMessage = Var.Create ""
        let uploadFiles (fileVar:Var<JavaScript.File array>) (progressVar:Var<int option>) (fileUploadId:Var<string option>) (cancelFunction:ref<unit->unit>)=
            let files = fileVar.Get()
            match files|>List.ofSeq with
            // if no file to be uploaded, do not send request, but still change the id for that field.
            // An empty string will be detected as no file uploaded. In prod, use DU!
            | [] -> fileUploadId.Set (Some "")
            | _ -> // We need to build a FormData and append the file to be uploaded
                let formData = JavaScript.FormData()
                files
                |> Array.iter (fun f -> formData.Append(f.Name,f))
                JavaScript.Console.Log(formData)

                // We will send an xhr request to upload the file
                let xhr = new JavaScript.XMLHttpRequest();
                // set the cancel function to abort this xhr
                cancelFunction.Value <- (fun () -> xhr.Abort() )
                // Compute progress
                // Problem: we have an Event, not a ProgressEvent, and load and total are not known
                xhr.Upload.Onprogress <-
                     (fun (ev) ->
                        let progressEvent = downcast ev : JavaScript.ProgressEvent
                        //JavaScript.Console.Log($"loaded =  {progressEvent.Loaded }")
                        //JavaScript.Console.Log($"total =  {progressEvent.Total }")
                        JavaScript.Console.Log((int ((float progressEvent.Loaded) / (float progressEvent.Total)*100.0)))
                        progressVar.Set( Some (int ((float progressEvent.Loaded) / (float progressEvent.Total)*100.0)))
                     )
                xhr.Onload <-
                    (fun ev ->
                        // This callback is sent when the upload is done
                        let response = xhr.Response
                        // The server returns a simple JSON. We use anonymous records here, but in prod we could/should
                        // use a proper record type shared by client and server.
                        let fileInfo:{|id:string|} = unbox (WebSharper.JavaScript.JSON.Parse (unbox response))
                        fileUploadId.Set (Some fileInfo.id)

                    )
                let uploadLink = "/uploads_with_xhr"
                // Do not set the content type here, it prevents the browser setting
                // data boundaries https://developer.mozilla.org/en-US/docs/Web/API/FormData/Using_FormData_Objects#sending_files_using_a_formdata_object
                // Now that we have defined all prerequisites like callback and formData, issue the xhr request
                xhr.Open("post",uploadLink)
                xhr.Send(formData)
        let fileInput =
            div
                []
                [
                    Client.Doc.InputType.File [attr.id "file1"; attr.name "uploadedFile";attr.multiple "multiple"] fileVar
                    button
                        [
                            on.click (fun el ev ->
                            let fileElt = (downcast el.PreviousSibling : WebSharper.JavaScript.HTMLInputElement)
                            fileElt.Value <- ""
                            )
                        ]
                        [text "clear files"]
                ]
        // Resetting a file input element is done by setting its value to ""
        // This can only be done by working with the element in javascript.
        // I wanted to reset the file input element only by setting the Var<File array> to an empty array.
        // To that end, I watch the changes on the Var, and if it is set to an empty array,
        // the value of the file input is set to "". To identify the file input, it is assigned a randomly generated data-*
        // attribute getting a random value. These are used with Document.QuerySelector
        let resettableFileInput attrs (var: Var<JavaScript.File array>) =
            let randomIntString factor =
                JavaScript.Math.Floor(JavaScript.Math.Random()*factor).ToString()
            let randomAttributeSuffix =  randomIntString 10000
            let randomString = randomIntString 10000
            var.View |> View.Sink
                (fun v ->
                    if Seq.isEmpty v then
                        let fileElt = (downcast JavaScript.JS.Document.QuerySelector($"[data-{randomAttributeSuffix}=\"{randomString}\"]"): WebSharper.JavaScript.HTMLInputElement)
                        fileElt.Value <- ""

                )
            Client.Doc.InputType.File (attrs |> Seq.append [|Attr.Create $"data-{randomAttributeSuffix}" randomString|] ) var

        let progressDisplay (progressVar:Var<int option>)=
                progressVar.View
                |> View.Map
                    ( function
                        | Some i ->
                            div
                                [ ]
                                [
                                    text  $"uploaded {(string i)}%%"
                                ]
                        | None -> Doc.Empty
                    )
        let uploadForm =
            div [] [
                text "This form first uploads the file, which the server saves at a temporary location, returning an identification of that location.
                      After that, the form fields are sent via RPC along with the id of the upload, allowing the server to link the upload to the form submission.  "
                br [] []
                Client.Doc.InputType.Text [attr.name "textField"] textVar
                resettableFileInput [attr.id "file1"; attr.name "uploadedFile";attr.multiple "multiple"] fileVar
                progressDisplay progress |> Client.Doc.EmbedView
                button [on.click (fun ev el -> progress.Set None; fileVar.Set [||])
                        attr.disabledDynPred (View.Const "")  (progress.View |> View.Map (function | None -> false | Some 100 -> false | Some _ -> true))
                       ]
                       [text "clear files"]
                resettableFileInput [attr.name "uploadedFile2"] file2Var
                progressDisplay progress2 |> Client.Doc.EmbedView
                button [on.click (fun ev el -> progress2.Set None; file2Var.Set [||])
                        attr.disabledDynPred (View.Const "") (progress2.View |> View.Map (function | None -> false | Some 100 -> false |Some _ -> true))
                       ]
                       [text "clear files"]
                button
                    [on.click (fun ev el ->
                        uploadFiles fileVar progress fileUploadId cancelUpload
                        uploadFiles file2Var progress2 file2UploadId cancelUpload2
                        uploadDone
                        |> View.Sink
                            (fun b ->
                              if b then
                                //Call rpc with upload info
                                // The RPC function is async, so wrap the call in an async {}
                                let id1 = fileUploadId.Get()
                                let id2 = file2UploadId.Get()
                                async{
                                    try
                                        // The RPC returns the string we will display as the result
                                        let! res = Server.HandleRpcFormAfterAjaxMultipleUploads (textVar.Get()) (["field1";"field2"]) ([id1;id2])
                                        resultMessage.Set res
                                    with
                                    | e -> resultMessage.Set "error"

                                    return ()
                                } |> Async.StartImmediate
                            )

                    )]
                    [text "submit by AJAX"]
                button [on.click (fun el ev -> ( cancelUpload.Value(); cancelUpload2.Value()))] [text "Cancel"]
                div [] [text resultMessage.V]
                div [] [textView (uploadDone|> View.Map (string) )]
            ]
        uploadForm