namespace wsupload

open WebSharper

type UploadRecord =
    { [<FormData>] textField: string
    }
type EndPoint =
    | [<EndPoint "/">] Home
    | [<EndPoint "/about">] About
    | [<EndPoint "POST /submit_form"; FormData("textField")>] SubmitForm of textField:string
    | [<EndPoint "POST /submit_form_as_record">] SubmitFormAsRecord of r:UploadRecord
    | [<EndPoint "POST /upload_with_xhr">] UploadWithXhr
    | [<EndPoint "POST /uploads_with_xhr">] MultipleUploadsWithXhr
