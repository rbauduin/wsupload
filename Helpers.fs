namespace wsupload

open WebSharper

module Helpers =

    let copyFirstFileTo (ctx:WebSharper.Sitelets.Context<_>) (directory:string) =
        // Next code line commented gives an error
        // The type 'Context<_>' does not define the field, constructor or member 'HttpContext'
        // let files = ctx.HttpContext().Request.Form.Files
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let files = httpContext.Request.Form.Files
        let file = files[0]
        let fileName = file.FileName
        let uploadPath = System.IO.Path.Combine(directory,fileName)
        use fileStream = new System.IO.FileStream(uploadPath,System.IO.FileMode.Create)
        file.CopyTo(fileStream)
        printfn "Uploaded file saved at %s" uploadPath
        uploadPath

    // This function handles the upload of files before the RPC call submitting the form. It does this by
    // - assigning a random id to the upload
    // - save the uploaded file to a directory identified by the random id
    // - return the id identifying the upload to the caller
    let copyXhrFirstFileToWaitRoom (ctx:WebSharper.Sitelets.Context<_>)  =
        let waitRoom = "/tmp"
        let id = string (System.Random.Shared.NextInt64())
        // Next code line commented gives an error
        // The type 'Context<_>' does not define the field, constructor or member 'HttpContext'
        // let files = ctx.HttpContext().Request.Form.Files
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let files = httpContext.Request.Form.Files
        let file = files[0]
        let fileName = file.FileName
        let waitDir = System.IO.Path.Combine(waitRoom,id)
        let dirInfo = System.IO.Directory.CreateDirectory(waitDir)
        let uploadPath = System.IO.Path.Combine(waitDir,fileName)
        use fileStream = new System.IO.FileStream(uploadPath,System.IO.FileMode.Create)
        file.CopyTo(fileStream)
        printfn "Uploaded file saved at %s" uploadPath
        id

    open Microsoft.AspNetCore.WebUtilities
    open System.IO
    let cleanupHeaderValue (s:string) =
        Microsoft.Net.Http.Headers.HeaderUtilities.RemoveQuotes(s).Value

    // Inspired by https://procodeguide.com/programming/file-upload-in-aspnet-core/#Add_Service-2
    let rec uploadFile(reader:MultipartReader)(section:MultipartSection)(directory:string) = async {
        let hasContentDispositionHeader,contentDisposition = System.Net.Http.Headers.ContentDispositionHeaderValue.TryParse(section.ContentDisposition)
        if (hasContentDispositionHeader) then
            let hasFileName = not <| System.String.IsNullOrEmpty(contentDisposition.FileName)
            let hasFileNameStar = not <| System.String.IsNullOrEmpty(contentDisposition.FileNameStar)
            if (contentDisposition.DispositionType.Equals("form-data") &&
               (hasFileName || hasFileNameStar))
            then
                let filePath = System.IO.Path.GetFullPath(directory)
                use fileStream = System.IO.File.Create(Path.Combine(filePath, cleanupHeaderValue contentDisposition.FileName))
                do! section.Body.CopyToAsync(fileStream)|> Async.AwaitTask;
                let! nextSection = reader.ReadNextSectionAsync() |> Async.AwaitTask
                if nextSection <> null then
                    return! uploadFile reader nextSection directory
                else
                    return true
            else return false
        else
            return false
    }

    // Handles multiple files per field
    let copyXhrAllFilesToWaitRoom (ctx:WebSharper.Sitelets.Context<_>)  = async {
        let waitRoom = "/tmp"
        let id = string (System.Random.Shared.NextInt64())
        let waitDir = System.IO.Path.Combine(waitRoom,id)
        let _dirInfo = System.IO.Directory.CreateDirectory(waitDir)
        // Next code line commented gives an error
        // The type 'Context<_>' does not define the field, constructor or member 'HttpContext'
        // let files = ctx.HttpContext().Request.Form.Files
        let httpContext:Microsoft.AspNetCore.Http.HttpContext = unbox (ctx.Environment.["WebSharper.AspNetCore.HttpContext"])
        let boundary =
            Microsoft.Net.Http.Headers.HeaderUtilities.RemoveQuotes(
                Microsoft.Net.Http.Headers.MediaTypeHeaderValue.Parse(httpContext.Request.ContentType).Boundary
            ).Value
        let reader = new MultipartReader(boundary, httpContext.Request.Body);
        let! section = reader.ReadNextSectionAsync() |> Async.AwaitTask
        let idAsync = async {
            try
                let! uploaded =  uploadFile reader section waitDir
                return id
            with
            | _ -> return ""
        }

        return! idAsync
    }

